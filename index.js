const imaps = require('imap-simple');
const convert = require("xml-js");
const config = {
    imap: {
        user: 'fewebcrsyn@gmail.com',
        password: 'Sistemas$2020$',
        host: 'imap.gmail.com',
        port: 993,
        tls: true,
        authTimeout: 3000
    }
};
 
imaps.connect(config).then(function (connection) {
 
    connection.openBox('INBOX').then(function () { // abrir la carpeta de inbox y leer los correos no leidos
 
        // Fetch emails from the last 24h
        let delay = 24 * 3600 * 1000;
        let yesterday = new Date();
        yesterday.setTime(Date.now() - delay);
        yesterday = yesterday.toISOString();
        let searchCriteria = ['UNSEEN', ['SINCE', yesterday]];
        let fetchOptions = { bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)'], struct: true };
 
        // retrieve only the headers of the messages
        return connection.search(searchCriteria, fetchOptions);
    }).then(function (messages) { // sino trae mensajes no devuelve nada
        console.log("mensaje ",messages)
        
        if(messages.length === 0){
            console.log("No hay mensajes");

            return null;
        } else {
            
            let attachments = [];
            //let idmessage
            /*messages.forEach(message => {

                connection.addFlags(message.attributes.uid, "\Seen", (err) => {
                    if (err){
                        console.log('NO se pudo marcar como leido');
                        console.log(err);
                    }
                    console.log("Se marcó como leído");
                    
                })
            })*/
            
            messages.forEach(function (message) {
      
                //console.log(message)
                var parts = imaps.getParts(message.attributes.struct);
                attachments = attachments.concat(parts.filter(function (part) {
                    return part.disposition && part.disposition.type.toUpperCase() === 'ATTACHMENT';
                }).map(function (part) {
                    // retrieve the attachments only of the messages with attachments
                    return connection.getPartData(message, part)
                        .then(function (partData) {

                            const ext = part.disposition.params.filename;
                            console.log("extensionm ", getFileExtension3(ext));
                            if(getFileExtension3(ext) == 'xml'){
                                return {
                                    filename: part.disposition.params.filename,
                                    data: partData
                                };
                            }
                    });
                        
                }));
            });
            return Promise.all(attachments);
        }
        
    }).then(function (attachments) {
        
        if(attachments){
            for(let file of attachments){
                if(!(typeof file === 'undefined') && !(typeof file.data === 'undefined')){

                    const xml = new Buffer.from(file.data,'base64').toString('ascii');
                    const result = convert.xml2json(xml, {compact: true, spaces: 4});
                    const data = JSON.parse(result);
                        
                    if(!(typeof data.FacturaElectronica === 'undefined') 
                        && !(typeof data.FacturaElectronica.Receptor === 'undefined')){
                            //guardar en la bd

                            console.log(data.FacturaElectronica.Emisor.Identificacion.Numero._text);
                    }

                    else if(!(typeof data.NotaCreditoElectronica === 'undefined')
                        && !(typeof data.NotaCreditoElectronica.Receptor === 'undefined')){
                            // guardar en la bd
                            console.log(data.NotaCreditoElectronica.Emisor.Identificacion.Numero._text);
                    }   
                     else {
                        console.log("El tipo de factura no esta permitida");
                    }
                }
            }
        };

        // traer las facturas



        //connection.end();
    })
    .catch(err => {
        console.log(err);
    })
});

function getFileExtension3(filename) {
    return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
  }

/*const IMAP = require("imap");
const MailParser = require("mailparser").MailParser;
const moment = require('moment');
var fs = require('fs'), fileStream;
const path = require("path");
const imapEmailDownload = function () {
return new Promise(async (resolve, reject) => {
    try {
        const imapConfig = {
            user: 'fewebcrsyn@gmail.com',
            password: 'Sistemas$2020$',
            host: 'imap.gmail.com',
            port: '993',
            tls: true,
            tlsOptions: {
                secureProtocol: 'TLSv1_method'
            }
        }
        const imap = IMAP(imapConfig);

        imap.once("ready", execute);
        imap.once("error", function (err) {
            console.error("Connection error: " + err.stack);
        });

        imap.connect();

        function execute() {
            imap.openBox("INBOX", false, function (err, mailBox) {
                if (err) {
                    console.error("error al abrir el inbox ",err);
                    return;
                }
                imap.search([["ON", moment().format('YYYY-MM-DD')]], function (err, results) {
                    if (!results || !results.length) { console.log("No unread mails"); imap.end(); return; }
                    /* mark as seen
                    imap.setFlags(results, ['\\Seen'], function(err) {
                        if (!err) {
                            console.log("marked as read");
                        } else {
                            console.log(JSON.stringify(err, null, 2));
                        }
                    });
                    console.log("resultados" , results);
                    var f = imap.fetch(results, { bodies: "" });
                    f.on("message", processMessage);
                    f.once("error", function (err) {
                        return Promise.reject(err);
                    });
                    f.once("end", function () {
                        imap.end();
                    });
                });
            });
        }

        function processMessage(msg, seqno) {
          
            var parser = new MailParser({ streamAttachments: true });
            parser.on("headers", function (headers) {
            });

            parser.on('data', data => {
                if (data.type === 'text') {
                    console.log(seqno);
                    console.log(data.text);  /* data.html
                }

            });
            let data = ""
            msg.on("body", function (stream) {
                stream.on("data", function (chunk) {
                    data = data + chunk.toString("utf8");
                    parser.write(chunk.toString("utf8"));
                });
                stream.on("end", (chunk) => {
                })
            });

            parser.on('attachment', async function (attachment, mail) {
                console.log("datos correo ",attachment);
                //path.join(__dirname, ruta);
                let filepath = path.join(__dirname, './descargas/') //'./descargas/';
                let output = fs.createWriteStream(filepath + attachment.fileName);
                
                attachment.stream.pipe(output).on("end", function () {
                    console.log("All the data in the file has been read");
                }).on("close", function (err) {
                    console.log("Stream has been cloesd.");
                });

            });

            msg.once("end", function () {
                // console.log("Finished msg #" + seqno);
                parser.end();
            });
        }
        resolve();
    } catch (error) {
        console.log("error", error);
        reject(error);
    }
});
};


imapEmailDownload().then(response => {
	console.log("response ",response);

})
.catch(err => {
	console.log("err",err)
})*/